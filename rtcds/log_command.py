"""handle rtcds log command, showing journalctl output"""
import sys
import subprocess

import click


@click.command()
@click.option("-f", "--follow", help="continuously update (follow) the logs", is_flag=True)
def log(follow):
    """
    show logs for system services
    """
    if follow:
        flags = "-f"
    else:
        flags = ""
    subprocess.run(f"sudo journalctl {flags}", shell=True,
                   stdin=sys.stdin,
                   stdout=sys.stdout,
                   stderr=sys.stderr)
