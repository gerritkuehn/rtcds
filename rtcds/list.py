import click

from .util import list_host_models, list_models


@click.option("-h", "--host", is_flag=True, default=False,
              help="only list models that run on this host")
@click.command(name="list",
               help="list models that run on this host OR list every model defined on the system")
def list_(host):
    """list systems for host"""
    if host:
        models = list_host_models()
    else:
        models = list_models()
    click.echo("\n".join(models))
