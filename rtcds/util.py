# a collection of generally useful functions
import os
import subprocess

from .env import environment as env


def systemd_unit_status(unit):
    """
    Return a systemd unit's status
    :param unit: The name of a unit
    :return: "ON" if unit is running, or else "FAILED" if it has failed, otherwise "OFF"
    """
    result = subprocess.run(["systemctl", "is-failed", unit], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    if result.returncode == 0:
        return "FAILED"
    result = subprocess.run(["systemctl", "is-active", unit], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    if result.returncode == 0:
        return "ON"
    else:
        return "OFF"


def epics_only_model(model):
    """
    Return True if model is an epics only model
    :param model: a model name
    :return: True if model is an epics only model
    """
    return model in env.EPICS_ONLY_MODELS.split()


def list_host_models():
    """
    Return a list of model names meant to be run on this host.

    If no models are set to run on this host, an empty list is returned, e.g. build servers, boot servers.

    :return: A list of model names meant to run on this host.
    """
    # first check  rtsystab
    if os.path.exists("/etc/rtsystab") and os.path.exists("/etc/rt_fe.sh"):
        cmd = ["/etc/rt_fe.sh"]
        result = subprocess.run(cmd, stdout=subprocess.PIPE, text=True)
        return result.stdout.strip().split()
    # next, if environment set up, use that
    if env.IOP_MODEL != "":
        return [env.IOP_MODEL] + env.USER_MODELS.strip().split()
    else:
        cmd = "systemctl list-units --all --full --plain --no-legend --no-pager rts@*.target".split()
        result = subprocess.run(cmd, stdout=subprocess.PIPE, text=True)
        # output lines look like
        # rts@x2omcpi.target loaded active active Advanced LIGO RTS target: x2omcpi"
        lines = [line.strip() for line in result.stdout.split("\n") if len(line.strip()) > 0]
        return [line.split()[-1] for line in lines]


def list_models():
    """
    Return a list of models meant to run on this host, but if no models are meant to run on this host, then
    a list of all models to be run on the IFO are returned.  Used to get build and install lists for build servers and
    boot servers.
    :return: A list of models.  If /etc/rtsystab doesn't exist, then return an empty list.
    """
    if os.path.exists("/etc/rtsystab") and os.path.exists("/etc/rt.sh"):
        cmd = ["/etc/rt.sh"]
        result = subprocess.run(cmd, stdout=subprocess.PIPE, text=True)
        return result.stdout.strip().split()
    else:
        return list_host_models()


def check_model_in_host(model: str):
    """
    Given a name of a model, return True if the model is meant to run on the current host.
    :param model: A model name
    :return: True if model is meant to run on the current host.
    """
    return (not env.CHECK_MODEL_IN_HOST) or model in list_host_models()


if __name__ == "__main__":
    print( "\n".join(list_models()))
