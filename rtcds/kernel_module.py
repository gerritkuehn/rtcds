import subprocess


class KernelModule(object):
    def __init__(self, name, size, dependent_count, dependents):
        self.name = name
        self.size = int(size)
        self.dependent_count = int(dependent_count)
        self.dependents = dependents

    def __repr__(self):
        return f"KernelModule(name={self.name},size={self.size},dependent_count={self.dependent_count},dependents={self.dependents})"


def _get_modules():
    """Process 'lsmod' to get hash of KernelModule objects keyed by name"""
    result = subprocess.run('lsmod',
                            capture_output=True,
                            check=True)

    outstr = result.stdout.decode()
    outlines = outstr.split('\n')
    modules = {}
    # skip header
    for line in outlines[1:]:
        values = line.split()
        if len(values) < 3:
            continue
        deps = []
        if len(values) >= 4:
            deps = values[3].split(',')
        mod = KernelModule(*values[:3], dependents=deps)
        modules[mod.name] = mod
    return modules


def is_module_loaded(modname):
    modules = _get_modules()
    return modname in modules


def remove_module(modname, log_callback=None):
    """Remove a kernel module.

    If log_callback is not None, call it with informative string

    Throws an exception if the module is not found.
    """
    def log(s):
        log_callback and log_callback(s)

    log(f"remove_module(modname={modname})")
    modules = _get_modules()
    if modname not in modules:
        log(f"remove_module(): module {modname} not found.")
        raise Exception(f"Could not remove kernel module {modname}.  Module not found.")
    mod = modules[modname]
    if len(mod.dependents) > 0 or mod.dependent_count != 0:
        log(f"remove_module(): module {modname} has dependents.  Not removing.")
        raise Exception(f"Could not remove kernel module {modname}.  Module has dependents.")

    completion = subprocess.run(["rmmod", modname])
    if completion.returncode != 0:
        ecode = completion.returncode
        log(f"remove_module(): invocation of rmmod on {modname} failed with error code {ecode}.")
        raise Exception(f"Could not remove kernel module {modname}. 'rmmod' exited with code {ecode}.")


def remove_module_recursively(modname, log_callback=None):
    """Remove a kernel module and all dependents"""
    to_remove = [modname]
    print(f"removing module {modname}")
    while len(to_remove) > 0:
        print(to_remove)
        modules = _get_modules()
        next_mod_name = to_remove[-1]
        if next_mod_name in modules:
            next_mod = modules[next_mod_name]
            if len(next_mod.dependents):
                for dep in next_mod.dependents:
                    if dep in to_remove:
                        to_remove.remove(dep)
                to_remove += next_mod.dependents
                continue
        to_remove.pop()
        print(f"next_mod_name={next_mod_name}:{type(next_mod_name)}")
        remove_module(next_mod_name)
