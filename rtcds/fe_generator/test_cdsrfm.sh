#!/bin/bash

# need to specify NETWORK_DIR in all such tests
# since defualt goes to /run/systemd/network
mkdir -p test/network
export NETWORK_DIR=test/network

export IS_DOLPHIN_NODE=true
export CDSRFM=true
export START_MODELS=true
export USE_DOLPHIN_DAEMON=true

export PYTHONPATH=../..
python3 -m rtcds.fe_generator dummy test dummy2