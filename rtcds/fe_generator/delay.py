from .systemd import SystemdService


class Delay(SystemdService):
    def __init__(self, name: str, start_time_s: int, stop_time_s: int):
        _name = f"rts-delay-{name}"
        self.start_time_s = start_time_s
        self.stop_time_s = stop_time_s

        unit_text = f"""[Unit]
Description=Delay for {start_time_s} seconds

[Service]
Type=oneshot
RemainAfterExit=yes
ExecStartPre=/bin/sleep {start_time_s}
ExecStart=/bin/echo '[startup] finished waiting for {name}'
ExecStop=/bin/sleep {stop_time_s}
ExecStopPost=/bin/echo '[shutdown] finished waiting for {name}'
"""

        super().__init__(_name, unit_text)


