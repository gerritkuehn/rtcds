from systemd import *
import os

targ_dir = "test"

os.makedirs(targ_dir, exist_ok = True)

serv1 = SystemdService("serv1")
serv2 = SystemdService("serv2.service")
targ1 = SystemdTarget("targ1")
targ2 = SystemdTarget("targ2.target")

serv2.binds_to(serv1)
targ1.add_unit(serv1)
targ2.wanted_by("asystem.target")
targ1.require_after(targ2)

output_units(targ_dir)