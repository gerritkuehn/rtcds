from .systemd import SystemdService


class CDSRFM(object):
    """
    Create cdsrfm units
    """
    def __init__(self):
        self.module = SystemdService("rts-cdsrfm-module")
        self.epics = SystemdService("rts-cdsrfm-epics")

        self.epics.require_after(self.module)
