from .systemd import SystemdService, SystemdNetwork, SystemdTarget
from .log import klog

class DAQStreaming(object):
    def __init__(self, options):
        if options["DAQ_STREAMING"]:
            if "DAQ_ETH_DEV" not in options:
                klog("DAQ_ETH_DEV must be specified in envrionment to configure DAQ streaming")
                raise Exception("DAQ_ETH_DEV must be specified in envrionment to configure DAQ streaming")
            if "DAQ_ETH_IP" not in options:
                klog("DAQ_ETH_IP must be specified in envrionment to configure DAQ streaming")
                raise Exception("DAQ_ETH_IP must be specified in envrionment to configure DAQ streaming")
            ts = 'transport_specifier'
            if not (ts in options and options[ts]):
                raise Exception(f"option '{ts}' must be determined when reading options")

            local_dc = SystemdService("rts-local_dc")
            transport = SystemdService(f"rts-transport@{options[ts]}")

            # no reason for transport to start until local_dc is working
            transport.after(local_dc)

            self.target = SystemdTarget("rts-transport", """[Unit]
Description=Start transport of data to DAQ
""")
            self.target.add_unit(local_dc, transport)

            # need a network definition file since puppet isn't able to set this anymore
            SystemdNetwork("rts-daq", f"""[Match]
Name={options['DAQ_ETH_DEV']}

[Link]
MTUBytes={options["DAQ_MTU"]}

[Network]
Description=Interface for streaming data to the DAQ
Address={options['DAQ_ETH_IP']}
""")

        else:
            self.target = None