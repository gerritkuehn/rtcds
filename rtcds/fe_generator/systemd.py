import os
import os.path as path

all_units = []


class SystemdUnit(object):
    """Represents a single systemd units"""
    def __init__(self, name: str, body=None):
        """
        :param name:
        :param body: when None, it's implied the systemd unit was installed from some other source.
        when a string, the string is used as contents for a unit file created for this unit.
        """
        global all_units
        self.name = name
        self._binds_to = set()
        self._wants = set()
        self._part_of = set()
        self._after = set()
        self._requires = set()
        self._wanted_by = set()
        self.body = body
        all_units.append(self)

    def wants(self, other):
        """
        This unit will want the other process to start (but is not required to start).
        :param other: the other process
        :return: None
        """
        assert isinstance(other, SystemdUnit)
        self._wants.add(other)

    def require_after(self, other):
        """
        Start self after other starts.  Other is required to start but self is not bound to quit if other quits
        :param other:
        :return:
        """
        assert isinstance(other, SystemdUnit)
        self._requires.add(other)
        self._after.add(other)

    def after(self, other):
        """
        Start self after other starts.
        :param other:
        :return:
        """
        assert isinstance(other, SystemdUnit)
        self._after.add(other)

    def wanted_by(self, unit_name):
        """Pass string of an external unit, eg multi-user.target.  That unit will want self.
        """
        self._wanted_by.add(unit_name)

    def output_body(self, target_dir, *args):
        """
        Output the body of the main unit file, if defined.

        :param target_dir: base dir where unit file goes
        :return:
        """
        if self.body is not None:
            _path = path.join(target_dir, self.name)
            with open(_path, "wt") as unit_file:
                unit_file.write(self.body)

    def binds_to(self, other):
        """
        Bind the other process to start and stop with this unit
        :param other: the other process will only start if this unit is started, and will stop if this unit is stopped
        :return: None
        """
        assert isinstance(other, SystemdUnit)
        self._binds_to.add(other)
        self._after.add(other)


class SystemdService(SystemdUnit):
    def __init__(self, name, body=None):
        suffix = ".service"
        self.stop_timeout = None
        if name.endswith(suffix):
            super().__init__(name, body)
        else:
            super().__init__(name + suffix, body)


class SystemdTarget(SystemdUnit):
    def __init__(self, name, body=None):
        suffix = ".target"
        if name.endswith(suffix):
            super().__init__(name, body)
        else:
            super().__init__(name + suffix, body)

    def add_unit(self, *units):
        """
        Add a unit to be started and stopped with this target
        :param unit:
        :return:
        """
        for unit in units:
            assert isinstance(unit, SystemdUnit)
            self._wants.add(unit)
            unit._part_of.add(self)


class SystemdNetwork(SystemdUnit):
    def __init__(self, name, body=None):
        suffix = ".network"
        if name.endswith(suffix):
            super().__init__(name, body)
        else:
            super().__init__(name + suffix, body)

    def output_body(self, target_dir, network_dir):
        """
        Output the body of the main unit file, if defined.

        :param target_dir: base dir where unit file goes
        :return:
        """
        super().output_body(network_dir)


# low level systemd unit file handling
gen_disclaimer = "# Created by systemd cds_frontend 'fe_generator'\n"

def wants(target_dir, unit_name, target_name):
    """
    Makes systemd unit unit_name wanted by target_name

    :return:
    """
    override_path = path.join(target_dir, f"{target_name}.d")
    os.makedirs(override_path, exist_ok=True)
    conf_path = path.join(override_path, f"wants_{unit_name}.conf")
    with open(conf_path, "wt") as f:
        f.write(gen_disclaimer)
        f.write(f"""[Unit]
Wants={unit_name}
""")


def part_of(target_dir, unit_name, parent_name):
    """
    Make unit_name a part of parent_name, so that when parent_name is stopped, unit_name is as well.
    """
    unit_d = path.join(target_dir, f"{unit_name}.d")
    os.makedirs(unit_d, exist_ok=True)
    fname = path.join(unit_d, f"part_of_{parent_name}.conf")
    with open(fname, "wt") as conf:
        conf.write(gen_disclaimer)
        conf.write(f"""[Unit]
PartOf={parent_name}
""")


def after(target_dir, after_name, before_name):
    """
    Make the systemd unit after_name start after before_name is finished
    """
    after_d = path.join(target_dir, f"{after_name}.d")
    os.makedirs(after_d, exist_ok=True)
    fname = path.join(after_d, f"after_{before_name}.conf")
    with open(fname, "wt") as conf:
        conf.write(gen_disclaimer)
        conf.write(f"""[Unit]
After={before_name}
""")


def requires(target_dir, after_name, before_name):
    """
    Make the systemd unit after_name require before_name is finished
    """
    after_d = path.join(target_dir, f"{after_name}.d")
    os.makedirs(after_d, exist_ok=True)
    fname = path.join(after_d, f"require_{before_name}.conf")
    with open(fname, "wt") as conf:
        conf.write(gen_disclaimer)
        conf.write(f"""[Unit]
Requires={before_name}
""")


def binds_to(target_dir, after_name, before_name):
    """
    Make the systemd unit after_name require before_name is finished
    """
    after_d = path.join(target_dir, f"{after_name}.d")
    os.makedirs(after_d, exist_ok=True)
    fname = path.join(after_d, f"bindsto_{before_name}.conf")
    with open(fname, "wt") as conf:
        conf.write(gen_disclaimer)
        conf.write(f"""[Unit]
BindsTo={before_name}
""")


def stop_delay(target_dir, service_name, delay_sec):
    """
    Set the delay between SIGTERM and SIGKILL when systemd is trying to stop a service.

    :param target_dir:
    :param service_name:
    :param delay_sec:
    :return:
    """
    conf_d = path.join(target_dir, f"{service_name}.d")
    os.makedirs(conf_d, exist_ok=True)
    fname = path.join(conf_d, f"stop_delay.conf")
    with open(fname, "wt") as conf:
        conf.write(gen_disclaimer)
        conf.write(f"""[Service]
TimeoutStopSec={delay_sec}
""")


def output_units(target_dir, network_dir):
    """
    Process created SystemdUnit objects and output the necessary files
    :param target_dir: where unit files ought to be installed
    :param network_dir: where network files ought to be installed
    :return:
    """

    os.makedirs(target_dir, exist_ok=True)
    os.makedirs(network_dir, exist_ok=True)

    global all_units
    for unit in all_units:
        for u2 in unit._binds_to:
            binds_to(target_dir, unit.name, u2.name)
        for u2 in unit._requires:
            requires(target_dir, unit.name, u2.name)
        for u2 in unit._after:
            after(target_dir, unit.name, u2.name)
        for u2 in unit._part_of:
            part_of(target_dir, unit.name, u2.name)
        for u2 in unit._wants:
            wants(target_dir, u2.name, unit.name)
        for before_name in unit._wanted_by:
            wants(target_dir, unit.name, before_name)

        # write out stop delay
        if isinstance(unit, SystemdService) and unit.stop_timeout is not None:
            stop_delay(target_dir, unit.name, unit.stop_timeout)

        # dump the body to a file if exists
        unit.output_body(target_dir, network_dir)