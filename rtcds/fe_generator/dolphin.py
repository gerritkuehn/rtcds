from .systemd import SystemdService, SystemdTarget
from .delay import Delay
from .log import klog

class Dolphin(object):
    def __init__(self, options):

        self.port_control = None

        self.last_driver = None

        self.driver_target = None

        # units to start on boot
        self.on_boot = []

        # bind points for IOP, cdsrfm module to bind to.
        self.after_binds = []

        # services follow-on units like IOP and cdsrfm should bind to
        # to make sure dolphin is loaded on startup

        dolph_gen = options["DOLPHIN_GEN"]

        self.target = None

        if options["IS_DOLPHIN_NODE"]:
            self.target = SystemdTarget("rts-dolphin", """[Unit]
Description=Start Dolphin Reflected Memory and associated units
""")

            if options["USE_DOLPHIN_PORT_CONTROL"]:
                dolph_port_service = SystemdService("rts-dolphin-port")
                dolph_port_delay = Delay("dolphin_port", 15, 5)
                dolph_port_delay.binds_to(dolph_port_service)
                self.port_control = dolph_port_delay
                self.target.add_unit(dolph_port_service, dolph_port_delay)

            kosif = SystemdService("dis_kosif")
            dolph = SystemdService(f"dis_{dolph_gen}")
            irm = SystemdService("dis_irm")
            sisci = SystemdService("dis_sisci")

            self.last_driver = sisci
            self.after_binds.append(sisci)

            nodemgr = SystemdService("dis_nodemgr")


            nodemgr.binds_to(sisci)
            sisci.binds_to(irm)
            irm.binds_to(dolph)
            dolph.binds_to(kosif)

            self.driver_target = SystemdTarget("rts-dolphin-driver", f"""[Unit]
Description=Dolphin {dolph_gen} drivers
""")

            self.driver_target.add_unit(kosif, dolph, irm, sisci, nodemgr)

            self.target.add_unit(self.driver_target)
            self.target.add_unit(nodemgr)

            klog(f"{dolph_gen} gen. dolphin drivers added")

            # wait until after port control if it's enabled
            if self.port_control is not None:
                kosif.binds_to(self.port_control)
                self.driver_target.require_after(self.port_control)


            # add dolphin daemon if needed
            if options["USE_DOLPHIN_DAEMON"]:
                klog("adding dolphin daemon")
                proxy = SystemdService("rts-dolphin-proxy","""[Unit]
Description=Kernel module server for the dolphin proxy daemon

[Service]
Type=oneshot
RemainAfterExit=yes
ExecStartPre=sleep 5
ExecStart=modprobe dolphin-proxy-km
ExecStop=rmmod dolphin-proxy-km
""")
                daemon = SystemdService("rts-dolphin_daemon")

                daemon.require_after(proxy)
                proxy.binds_to(self.last_driver)
                proxy.require_after(nodemgr)
                daemon_delay = Delay("dolphin_daemon", 5, 5)
                daemon_delay.binds_to(daemon)
                self.after_binds.append(daemon_delay)

                self.target.add_unit(daemon, proxy)
