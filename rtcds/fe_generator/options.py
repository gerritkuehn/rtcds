# gather options for generating systemd units
import os
import os.path as path


def split(s):
    """Split a string on whitespace"""
    return s.split()


def boolean(s):
    """Convert a string into True or False.
    Return true only if first character is a 'T' of either case"""
    return s[0] == "t" or s[0] == "T"


class Variable(object):
    def __init__(self, convert, default=None):
        self.convert = convert
        self.default = default


# net every expected variable is set up here, only those that need translation from a string
# or need a default value
variables = {
    'IOP_MODEL': Variable(str, None),
    'USER_MODELS': Variable(split, default=[]),
    'EPICS_ONLY_MODELS': Variable(split, default=[]),
    'EDC': Variable(split, default=[]),
    'IS_DOLPHIN_NODE': Variable(boolean, False),
    'DOLPHIN_GEN': Variable(str, "ix"),
    'USE_DOLPHIN_PORT_CONTROL': Variable(boolean, True),
    'USE_DOLPHIN_DAEMON': Variable(boolean, False),
    'DAQ_STREAMING': Variable(boolean, False),
    'CDSRFM': Variable(boolean, False),
    'START_MODELS': Variable(boolean, False),
    'DAQ_MTU': Variable(int, 9000),
    'NETWORK_DIR': Variable(str, "/run/systemd/network")
}


def get_options():
    # first check /etc/advligorts/systemd_env.. files
    options = {key: value for key, value in os.environ.items()}
    host_name = os.uname()[1]

    # also if rtsystab is available, prefer its unit list

    read_rtsystab(f"/etc/rtsystab", options, host_name)

    # process the options into right type, etc, and create a few that aren't to be found in the files
    for name, var in variables.items():
        if name in options:
            try:
                options[name] = var.convert(options[name])
            except:
                options[name] = var.default
        else:
            options[name] = var.default

    if options['IOP_MODEL'] in options['EPICS_ONLY_MODELS']:
        options['IOP_MODEL'] = None

    options['USER_MODELS'] = [m for m in options['USER_MODELS']
                              if m not in options['EPICS_ONLY_MODELS']]

    options['HAS_DOLPHIN_PORT'] = path.exists('/etc/dolphin_ix_port_ctl.sh')
    options['HAS_EPICS_ONLY_MODELS'] = len(options['EPICS_ONLY_MODELS']) > 0
    options['HAS_IOP_MODEL'] = options['IOP_MODEL'] is not None
    options['HAS_USER_MODELS'] = len(options['USER_MODELS']) > 0

    options['HAS_EDC'] = "EDC" in options and len(options["EDC"]) > 0

    if 'cps_xmit_args' in options:
        options['transport_specifier'] = 'cps_xmit'

    return options


def read_rtsystab(fname, options, host_name):
    try:
        with open(fname, "rt") as f:
            for line_raw in f.readlines():
                line = line_raw.strip()
                if len(line) <= 0:
                    continue
                line = line.split('#')[0].strip()
                if len(line) <= 0:
                    continue
                words = [word.strip() for word in line.split()]
                if len(words) > 1 and words[0] == host_name:
                    options["IOP_MODEL"] = words[1]
                    options["USER_MODELS"] = " ".join(words[2:])
                    return
    except IOError:
        pass
