"""Front end sequencer helpers and object for starting the dolphin daemon"""

from os import path


def create_dolphin_km_unit_file(target_dir):
    file_path = path.join(target_dir, "rts-dolphin-proxy.service")

    with open(file_path, "wt") as f:
        f.write("""[Unit]
    Description=Kernel module server for the dolphin proxy daemon

    [Service]
    Type=oneshot
    RemainAfterExit=yes
    ExecStartPre=sleep 5
    ExecStart=modprobe dolphin-proxy-km
    ExecStop=rmmod dolphin-proxy-km
    """)
