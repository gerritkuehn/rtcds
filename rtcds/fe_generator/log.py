
def klog(line):
    """
    Write the line to dmesg
    :param line:
    :return:
    """
    try:
        with open("/dev/kmsg", "wt") as f:
            f.write(f"fe_generator: {line}\n")
    except PermissionError:
        print(line)

