# Setup the various processes for the front end startup

import os
import os.path as path




class Process(object):
    def __init__(self, start, end=None, first_service=None):
        self.start = start
        if end is None:
            self.end = start
        else:
            self.end = end
        if first_service is None:
            self.first_service = start
        else:
            self.first_service = first_service


class FrontEndProcesses(object):
    def __init__(self, target_dir):
        self.target_dir = target_dir

    def models(self):
        target_name = path.join(self.target_dir, "rts-models.target")
        with open(target_name, "wt") as f:
            f.write(gen_disclaimer)
            f.write("""[Unit]
Description=All models
""")

    @staticmethod
    def dolphin_port():
        return Process('rts-dolphin-port.service', 'rts-dolphin-port.service')

    def dolphin_drivers(self, dolphin_gen: str):
        """Create units for dolphin drivers.

        :param dolphin_gen: The two letter generation name for the Dolphin rfm in use
        """
        # link up from one to the next
        targ_name = "rts-dolphin-driver.target"
        targ_path = path.join(self.target_dir, targ_name)
        services = ['dis_kosif.service', f"dis_{dolphin_gen}.service", 'dis_irm.service', 'dis_sisci.service',
                    'dis_nodemgr.service']

        with open(targ_path, "wt") as f:
            f.write(gen_disclaimer)
            f.write(f"""[Unit]
Description=Dolphin IX drivers
Wants={" ".join(services)}""")

        self.serialize_units(services)
        self.bind_units(services)

        for service in services:
            self.part_of(service, targ_name)

        return Process(targ_name, 'dis_nodemgr.service', 'dis_kosif.service',)

    def epics_only_models(self, models):
        services = [f"rts-epics@{model}.service" for model in models]

        target_name = "rts-epics-only-models.target"
        target_path = path.join(self.target_dir, target_name)
        with open(target_path, "wt") as f:
            f.write(gen_disclaimer)
            f.write(f"""[Unit]
Description=All epics only models
Wants={" ".join(services)}
""")

        self.serialize_units(services)
        for service in services:
            self.part_of(service, target_name)

        self.link_to(target_name, "rts-models.target")
        self.part_of(target_name, "rts-models.target")

        return Process(target_name, services[-1], services[0])

    def iop_model(self, model, bind_to_dolphin=True):
        """
        Link up various services for the IOP model.
        If bind_to_dolphin is true, link the iop to the appropriate dolphin service
        """
        target_path = path.join(self.target_dir, "rts-iop-model.target")
        with open(target_path, "wt") as f:
            f.write(gen_disclaimer)
            f.write(f"""[Unit]
Description=The IPO model.
Wants=rts@{model}.target
""")

        self.part_of(f"rts@{model}.target", "rts-iop-model.target")

        self.link_to("rts-iop-model.target", "rts-models.target")
        self.part_of("rts-iop-model.target", "rts-models.target")

        if bind_to_dolphin:
            self.after(f"rts-module@{model}.service", "dis_nodemgr.service")
            self.requires(f"rts-module@{model}.service", "dis_nodemgr.service")

        return Process(f"rts-iop-model.target", f"rts-awgtpman@{model}.service",
                       f"rts-epics@{model}.service")

    def user_models(self, models, iop_model):
        target_name = "rts-user-models.target"
        target_path = path.join(self.target_dir, target_name)
        with open(target_path, "wt") as f:
            f.write(gen_disclaimer)
            f.write("""[Unit]
Description=All user models
""")

        services = []
        for model in models:
            services += [f"rts-epics@{model}.service", f"rts-module@{model}.service",
                         f"rts-awgtpman@{model}.service"]

        self.serialize_units(services)

        for model in models:
            self.link_to(f"rts@{model}.target", target_name)
            self.part_of(f"rts@{model}.target", target_name)

            # make sure iop is running before running any user models
            self.requires(f"rts-epics@{model}.service", f"rts-module@{iop_model}.service")
            self.after(f"rts-epics@{model}.service", f"rts-module@{iop_model}.service")
            self.binds_to(f"rts-module@{model}.service", f"rts-module@{iop_model}.service")
            self.after(f"rts-module@{model}.service", f"rts-module@{iop_model}.service")

        self.link_to(target_name, "rts-models.target")
        self.part_of(target_name, "rts-models.target")

        return Process(f"rts-user-models.target", services[-1],
                       services[0])

    def edcs(self, edcs):
        """
        Takes a list of edc names and returns a Process object that can be used to start them all.
        """
        services = [f"rts-edc_{edc}.service" for edc in edcs]
        self.serialize_units(services)
        return Process(services[0], services[-1])

    def streaming(self, options):
        # check transport specifier exists
        ts = 'transport_specifier'
        if not (ts in options and options[ts]):
            raise Exception(f"option '{ts}' must be determined when reading options")

        # set up port
        network_dir = "/run/systemd/network"
        os.makedirs(network_dir, exist_ok=True)
        fpath = path.join(network_dir, "rts-daq.network")
        with open(fpath, "wt") as f:
            f.write(gen_disclaimer)
            f.write(f"""[Match]
Name={options['DAQ_ETH_DEV']}

[Link]
MTUBytes=9000

[Network]
Description=Interface for streaming data to the DAQ
Address={options['DAQ_ETH_IP']}
""")

        # setup services
        services = ["rts-local_dc.service", f"rts-transport@{options[ts]}.service"]
        self.serialize_units(services)

        # setup target
        targ_unit_name = "rts-transport.target"
        targ_path = path.join(self.target_dir, targ_unit_name)
        with open(targ_path, "wt") as f:
            f.write(gen_disclaimer)
            f.write(f"""[Unit]
Description=Start transport of data to DAQ
Wants={" ".join(services)}
""")

        for service in services:
            self.link_to(service, targ_unit_name)
            self.part_of(service, targ_unit_name)

        return Process(targ_unit_name, services[-1], services[0])

    def serialize_units(self, services):
        """
        Take a list of systemd unit names and put one after the other.
        """
        for i in range(1, len(services)):
            self.after(services[i], services[i-1])

    def bind_units(self, services):
        """
        Take a list of systemd units and bind them together, with second
        bound to the first, third bound to the second etc.
        """
        for i in range(1, len(services)):
            self.binds_to(services[i], services[i-1])

    def serialize_processes(self, processes):
        """
        Take a list of processes and put one after the other
        """
        for i in range(1, len(processes)):
            self.after(processes[i].first_service, processes[i-1].end)

    def create_world_target(self):
        with open(path.join(self.target_dir, "rts-world.target"), "wt") as world:
            world.write(gen_disclaimer)
            world.write("""[Unit]
Description=All model and streaming services for Front End servers

[Install]
WantedBy=multi-user.target
""")

