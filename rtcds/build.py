"""
build a model
"""
import os
from os import path
import sys
import asyncio
import subprocess

import click
from click import ClickException

from .env import environment as env
from .util import list_models
from .log import log, error
from .install import install_body, install_summary


build_tasks = {}

promise_hash = {}

def create_build_directory():
    """
    Create the build directory if necessary

    :return: the build path
    """

    global env

    build_path = env.RCG_BUILDD
    if check_configured(build_path):
        return build_path

    if path.lexists(build_path):
        raise ClickException(f"build path '{build_path}' exists but is not configured properly.  Delete this directory and rerun, or change build path")

    rcg_path = env.RCG_SRC

    log(f"creating build directory {build_path}")
    os.makedirs(build_path, mode=0o775)

    log(f"configuring build directory {build_path}")

    os.chdir(build_path)

    conf = path.join(rcg_path, 'configure')
    subprocess.run([conf])
    return build_path


# set some defaults from environ
kernel_mode_default = env.USE_KERNEL_MODELS
user_mode_default = not kernel_mode_default


class BuildArgs:
    """ Class to manage build arguments for rtcds"""
    def __init__(self, build_user_space, build_kernel_space, build_librts):
        self.build_user_space = build_user_space
        self.build_kernel_space = build_kernel_space
        self.build_librts = build_librts

    def get(self):
        """
        Return list of arguments to pass to make when building models
        :return:  a list of arguments for make <model>
        """
        args = []
        if self.build_user_space:
            args.append("RCG_BUILD_USP=YES")
        if self.build_librts:
            args.append("RCG_BUILD_LIBRTS=YES")
        if not self.build_kernel_space:
            args.append("RCG_BUILD_NO_KOBJ=YES")
        return args


def build_model(model: str, build_args: BuildArgs, direct_output: bool):
    """
    Build one model
    :param model: The name of the model
    :param user_space: when true, build user-mode model
    :param kernel_space: when true, build kernel-mode model
    :param direct_output: when true, pipe output of build immediately to stdout
    :return: the async subprocess that builds the model
    """
    global env
    build_path = env.RCG_BUILDD
    os.chdir(build_path)
    args = build_args.get()
    cmd = tuple(['make', model] + args)

    if direct_output:
        stdout = None
    else:
        stdout = asyncio.subprocess.PIPE

    return asyncio.create_subprocess_exec(*cmd, env=env.get_environment(), stdout=stdout,
                                                stderr=asyncio.subprocess.STDOUT)


# def build_all(build_args: BuildArgs):
#     """
#     Build all models for the given host, or build all models in the whole IFO if run on a boot server or build server
#     :param user_space: When true, build user space models
#     :param kernel_space: When true, build kernel space models
#     :return: None
#     """
#     global env
#     os.chdir(env.RCG_BUILDD)
#     log(f"### building all models")
#     args = build_args.get()
#     cmd = ['make', '-i', 'World'] + args
#     subprocess.run(cmd, env=env.get_environment())


def check_configured(build_dir: str):
    """
    Return true if build_dir is already configured to build models.

    :param build_dir:
    :return:
    """
    if not path.isdir(build_dir):
        return False
    if not path.exists(path.join(build_dir, 'Makefile')):
        return False
    return True


async def wait_for_procs(direct_output, parallel, simpleoutput):
    """
    Queue up promises in promise hash.
    Run parallel number of processes from queue.
    Wait until queue is empty and all Process objects in bulit_tasks are terminated
    keys are names of process
    :return:
    """
    global build_tasks
    global promise_hash

    total = len(promise_hash)
    run_queue = list(promise_hash.keys())
    run_queue.reverse()
    index = 0
    left = total
    num_failed = 0
    num_succeed = 0

    # process that are currently running
    proc_hash = {}

    building = []

    while True:

        # check if anything left to do
        if len(run_queue) == 0 and len(building) == 0:
            break

        # add more running processes if needed
        while len(run_queue) and (parallel == 0 or (len(building) < parallel)):
            next_mod = run_queue.pop()
            proc = await promise_hash[next_mod]
            proc.output = ""
            build_tasks[next_mod] = proc
            building.append(next_mod)

        if len(building) <= 0:
            raise Exception("build list ran dry while run queue was not empty")

        check_model = building[index]
        proc = build_tasks[check_model]

        try:
            output = await asyncio.wait_for(proc.communicate(), timeout=0.5)

        except asyncio.TimeoutError:
            pass
        else:
            if proc.returncode == 0:
                if simpleoutput and not direct_output:
                    print(f"{check_model} build succeeded")
                num_succeed += 1
            else:
                if simpleoutput and not direct_output:
                    print(f"{check_model} build failed")
                num_failed += 1
            building.remove(check_model)

            if not direct_output:
                output = output[0].decode()
                proc.output = output

        if len(building) and not direct_output and not simpleoutput:
            report_line = []
            if len(run_queue) > 0:
                report_line.append( f"{len(run_queue)} queued")
            if num_succeed > 0:
                report_line.append(f"{num_succeed} done")
            if num_failed > 0:
                report_line.append(f"{num_failed} failed")
            report_line.append(f"{len(building)} building")
            sys.stdout.write(", ".join(report_line) + f" [{check_model}]" + 20*" " + "\r")

        index += 1
        if index >= len(building):
            index = 0
    print()


async def build_models(models, build_args: BuildArgs, parallel: int, direct_output: bool, simpleoutputs: bool):
    """
    Build a list of models
    :param build_args:
    :param asynchronous: If True, build each model in a parallel background task
    :param direct_output: If True, pipe output directly to stdout
    :return: None
    """

    global env, build_tasks, promise_hash
    env.check_environment()
    if os.getuid() == 0:
        error("building as 'root' not allowed")

    build_path = create_build_directory()
    os.chdir(build_path)

    print(f"Building {len(models) == 1 and 'this model' or 'these models'}: {' '.join(models)}")

    for m in models:
        if m.lower() != m:
            click.echo(f"*** Error: {m} has uppercase letters. Model names must not have uppercase letters.")
        else:
            build_promise = build_model(m, build_args, direct_output)
            promise_hash[m] = build_promise
    await wait_for_procs(direct_output, parallel, simpleoutputs)


def summarize_output(build_promises, build_procs, nooutputs, autoanswer):
    """
    generate a report to the screen of build results
    :param build_procs: a hash of model_name : asyncio.Process object that should be finished
    :param nooutputs: if True, don't print outputs of failed builds
    :return : list of successful models
    """
    incomplete = []
    success = []
    failed = []

    total = len(build_promises)

    for model, proc in build_procs.items():
        if proc.returncode is None:
            incomplete.append(model)
        elif proc.returncode == 0:
            success.append(model)
        else:
            failed.append(model)

    queued = [m for m in build_promises if m not in build_procs]

    print(f"\nSummary of {total} model builds")
    if len(success) > 0:
        models = " ".join(success)
        print(f"{len(success)} of {total} model builds were successful: {models}")
    if len(failed) > 0:
        models = " ".join(failed)
        print(f"{len(failed)} of {total} model builds failed: {models}")
    if len(incomplete) > 0:
        models = " ".join(incomplete)
        print(f"{len(incomplete)} of {total} model builds were started but not completed: {models}")
    if len(queued) > 0:
        print(f"{len(queued)} of {total} models were queued and never built: {' '.join(queued)}")

    if len(failed) > 0 and not nooutputs:
        if total > 1:
            show_failures = None
            while show_failures is None:
                if autoanswer:
                    out_in = "Y"
                else:
                    out_in = input("\nShow output from failed builds [Y/n]? ").strip()
                if len(out_in) > 0 and out_in[0] in ['n', 'N']:
                    show_failures = False
                elif len(out_in) == 0 or out_in[0] in ['y', 'Y']:
                    show_failures = True
        else:
            show_failures = True
        if show_failures:
            first = True
            for m in failed:
                if not first and not autoanswer:
                    input(f"\nPress <enter> to show output for {m}")
                first = False
                print(f"\nOutput of build of {m}")
                print(build_procs[m].output)
    return success


# def cleanup():
#     """
#     Clean up any asynch tasks when exiting suddenly
#     :return:
#     """
#     print("======= cleaning up build ==========")
#     global build_tasks
#     for proc in build_tasks.values():
#         try:
#             proc.terminate()
#         except ProcessLookupError:
#             pass
#     loop = asyncio.get_event_loop()
#     if loop and loop.is_running():
#         loop.create_task(wait_for_procs(build_tasks))
#     else:
#         asyncio.run(wait_for_procs(build_tasks)


def build_body(model, user_space, kernel_space, build_librts, parallel, directout, simpleoutputs):
    """
    Body of the build command, separated to help with re-use.

    :return : True if all models built successfully
    """

    build_args = BuildArgs(build_user_space=user_space, build_kernel_space=kernel_space, build_librts=build_librts)

    loop = asyncio.get_event_loop()

    try:
        successful_models = loop.run_until_complete(build_models(model, build_args, parallel, directout, simpleoutputs))
    except KeyboardInterrupt:
        print("\nInterrupted from the keyboard")

    global build_tasks

    return all([p.returncode == 0 for p in build_tasks.values()])


@click.command()
@click.argument("model", nargs=-1
                )
@click.option("-a", "--all", "all_", default=False, is_flag=True,
              help='build all models'
              )
@click.option("--userland/--no-userland", "--user-space/--no-user-space", "user_space",
              default=user_mode_default, help="do not build model in user space",
              )
@click.option("--librts", "build_librts", is_flag=True, show_default=True,
              default=False, help="Build the librts library and python bindings",
              )
@click.option("--kernel-space/--no-kernel-space", "kernel_space",
              help="do not build model in user space",
              default=kernel_mode_default)
@click.option("-a", "--async", "asynchronous", default=False, is_flag=True,
              help="build multiple models simultaneously.  Same as '-p 0'")
@click.option("-d", "--direct-output","directout", default=False, is_flag=True, help="pipe output from builds directly to stdout, implies --no-outputs.  The default for standard builds")
@click.option("-b", "--buffered-output", "bufferedout", default=False, is_flag=True, help="output of failed builds only are shown when all builds are finished.  The default when building asynchronously.")
@click.option("-p", "--parallel", default=1, help="Number of models to build in parallel at one time. 0=build all models at once, which may eat up memory!")
@click.option("-n", "--no-outputs", "nooutputs", is_flag=True, help="skip printing of outputs of failed builds")
@click.option("-s", "--simple-output", "simpleoutputs", is_flag=True, help="replace nice status line when building with simple notice when builds are done")
@click.option("-i", "--install", is_flag=True, default=False, help="if builds are successful, the newly built models will be installed")
@click.option("-y","--yes", "autoanswer", is_flag=True, default=False, help="auto-answer any question with the default answer")
def build(model, all_, user_space, kernel_space, build_librts, asynchronous, directout, bufferedout, parallel, nooutputs, simpleoutputs, install, autoanswer):
    """
    Build one or more models

    MODEL: the name of the model to build
    """
    if asynchronous and parallel == 1:
        parallel = 0
    if directout and bufferedout:
        raise ClickException("--direct-output and --buffered-output are not compatible.  Select only one of these options.")
    elif bufferedout:
        directotout = False
    elif parallel == 1:
        directout = True
    if directout:
        nooutputs = True


    if directout and simpleoutputs:
        print("WARNING: --simple-output has no effect if --direct-output is used. \n\t --direct-output is the default when building models sequentially.")

    if all_:
        model = list_models()

    all_built = build_body(model, user_space, kernel_space, build_librts, parallel, directout, simpleoutputs)

    if install:
        if all_built:
            print("\nBuild success.  Proceeding with install")
            install_body(model)
        else:
            print("\ninstallation skipped because there were some build errors")

    global build_tasks, promise_hash
    summarize_output(promise_hash, build_tasks, nooutputs, autoanswer)
    num_failed_install = 0
    if install:
        num_failed_install = install_summary()

    if (not all_built) or num_failed_install > 0:
        sys.exit(1)